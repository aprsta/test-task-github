package org.example.task

import org.example.task.common.di.component.DaggerSingletonComponent
import org.example.task.common.di.module.ContextModule
import org.example.task.common.di.component.SingletonComponent

class Application : android.app.Application() {
    companion object {
        lateinit var component: SingletonComponent
            private set
    }

    override fun onCreate() {
        super.onCreate()

        component = DaggerSingletonComponent.builder()
                .contextModule(ContextModule(this))
                .build()
    }
}