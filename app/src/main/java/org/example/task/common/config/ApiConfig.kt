package org.example.task.common.config

object ApiConfig {
    const val GITHUB_API_PROTOCOL = "http"
    const val GITHUB_API_HOST = "api.github.com"
    const val GITHUB_API_PORT = 80
    const val GITHUB_API_URL = "$GITHUB_API_PROTOCOL://$GITHUB_API_HOST:$GITHUB_API_PORT"

    const val HEADER_ACCEPT = "Accept: application/json"
    const val HEADER_CONTENT = "Content-Type: application/json"

    const val TIMEOUT_MILLISEC: Long = 5000
    const val CACHE_SIZE: Long = 5 * 1024 * 1024

    const val GITHUB_ITEMS_PER_PAGE = 40
    const val GITHUB_ITEMS_PER_MAX_ITEMS = 1000
}