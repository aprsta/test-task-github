package org.example.task.common.config

object TransmittableKeys {
    const val GITHUB_NAME = "GITHUB_NAME"
    const val GITHUB_AVATAR_URL = "GITHUB_AVATAR_URL"
}