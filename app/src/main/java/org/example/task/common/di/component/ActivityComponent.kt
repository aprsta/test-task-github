package org.example.task.common.di.component

import dagger.Component
import org.example.task.common.di.module.PresenterModule
import org.example.task.common.di.scope.ActivityScope
import org.example.task.mvp.view.DetailsActivity
import org.example.task.mvp.view.SearchActivity

@ActivityScope
@Component(dependencies = [SingletonComponent::class], modules = [PresenterModule::class])
interface ActivityComponent {
    fun inject(searchActivity: SearchActivity)
    fun inject(detailsActivity: DetailsActivity)
}