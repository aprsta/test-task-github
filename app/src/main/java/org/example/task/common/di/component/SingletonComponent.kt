package org.example.task.common.di.component

import android.content.Context
import dagger.Component
import org.example.task.common.di.module.ContextModule
import org.example.task.common.di.module.NetworkModule
import org.example.task.mvp.model.network.NetworkApi
import org.example.task.mvp.presenter.DetailsPresenter
import org.example.task.mvp.presenter.SearchPresenter
import javax.inject.Singleton

@Singleton
@Component(modules = [ContextModule::class, NetworkModule::class])
interface SingletonComponent {
    fun provideContext(): Context
    fun provideNetworkApi(): NetworkApi

    fun inject(searchPresenter: SearchPresenter)
    fun inject(detailsPresenter: DetailsPresenter)
}