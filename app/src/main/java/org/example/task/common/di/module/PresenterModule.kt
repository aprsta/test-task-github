package org.example.task.common.di.module

import dagger.Module
import dagger.Provides
import org.example.task.common.di.scope.ActivityScope
import org.example.task.mvp.contract.DetailsContract
import org.example.task.mvp.contract.SearchContract
import org.example.task.mvp.presenter.DetailsPresenter
import org.example.task.mvp.presenter.SearchPresenter

@Module
class PresenterModule {
    @ActivityScope
    @Provides
    fun provideSearchPresenter(searchPresenter: SearchPresenter): SearchContract.Presenter {
        return searchPresenter
    }

    @ActivityScope
    @Provides
    fun provideDetailsPresenter(detailsPresenter: DetailsPresenter): DetailsContract.Presenter {
        return detailsPresenter
    }
}