package org.example.task.mvp.contract

import android.graphics.drawable.Drawable
import android.support.annotation.StringRes
import android.support.annotation.UiThread
import org.example.task.mvp.presenter.base.IMvpPresenter
import org.example.task.mvp.view.base.IMvpView

interface DetailsContract {
    interface View : IMvpView {
        @UiThread
        fun initialize()

        @UiThread
        fun showProgressBar()

        @UiThread
        fun hideProgressBar()

        @UiThread
        fun showAvatar(drawable: Drawable)

        @UiThread
        fun showName(name: String)

        @UiThread
        fun showErrorMessage(@StringRes messageId: Int)
    }

    interface Presenter : IMvpPresenter<View> {
        fun initialize(name: String, avatarUrl: String)
    }
}