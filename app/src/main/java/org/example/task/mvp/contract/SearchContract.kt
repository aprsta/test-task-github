package org.example.task.mvp.contract

import android.support.annotation.StringRes
import android.support.annotation.UiThread
import org.example.task.mvp.model.data.SearchItem
import org.example.task.mvp.model.data.SearchOwner
import org.example.task.mvp.presenter.base.IMvpPresenter
import org.example.task.mvp.view.base.IMvpView

interface SearchContract {
    interface View : IMvpView {
        @UiThread
        fun initialize()

        @UiThread
        fun showProgressBar()

        @UiThread
        fun hideProgressBar()

        @UiThread
        fun showPages()

        @UiThread
        fun hidePages()

        @UiThread
        fun clearResult()

        @UiThread
        fun clearPages()

        @UiThread
        fun searchResult(query: String, items: List<SearchItem>)

        @UiThread
        fun searchPages(query: String, items: List<Int>)

        @UiThread
        fun details(name: String, avatarUrl: String)

        @UiThread
        fun showErrorMessage(@StringRes messageId: Int)
    }

    interface Presenter : IMvpPresenter<View> {
        fun search(query: String)
        fun search(query: String, page: Int)
        fun details(owner: SearchOwner)
    }
}