package org.example.task.mvp.model.data

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class SearchItem(
        @SerializedName("full_name")
        @Expose
        val name: String? = null,

        @SerializedName("language")
        @Expose
        val language: String? = null,

        @SerializedName("owner")
        @Expose
        val owner: SearchOwner? = null
)