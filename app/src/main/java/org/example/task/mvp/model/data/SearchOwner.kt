package org.example.task.mvp.model.data

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class SearchOwner(
        @SerializedName("login")
        @Expose
        val name: String? = null,

        @SerializedName("avatar_url")
        @Expose
        val avatarUrl: String? = null
)