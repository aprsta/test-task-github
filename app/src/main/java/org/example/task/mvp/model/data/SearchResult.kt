package org.example.task.mvp.model.data

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class SearchResult (
        @SerializedName("total_count")
        @Expose
        val total: String? = null,

        @SerializedName("items")
        @Expose
        val items: List<SearchItem>? = null
)