package org.example.task.mvp.model.network

import io.reactivex.Observable
import okhttp3.ResponseBody
import org.example.task.common.config.ApiConfig
import org.example.task.mvp.model.data.SearchResult
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query
import retrofit2.http.Url

interface GithubApi {
    @Headers(ApiConfig.HEADER_ACCEPT, ApiConfig.HEADER_CONTENT)
    @GET("/search/repositories")
    fun searchRepositories(@Query("q") query: String, @Query("per_page") perPage: Int, @Query("page") page: Int): Observable<SearchResult>

    @GET
    fun downloadAvatar(@Url url: String): Observable<ResponseBody>
}