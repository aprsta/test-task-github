package org.example.task.mvp.model.network

import io.reactivex.Observable
import okhttp3.ResponseBody
import org.example.task.common.config.ApiConfig
import org.example.task.mvp.model.data.SearchResult

class NetworkApi(private val githubApi: GithubApi) {
    fun asyncSearchRepository(query: String): Observable<SearchResult> {
        return asyncSearchRepository(query, 1)
    }

    fun asyncSearchRepository(query: String, page: Int): Observable<SearchResult> {
        return githubApi.searchRepositories(query, ApiConfig.GITHUB_ITEMS_PER_PAGE, page)
    }

    fun asyncDownloadAvatar(avatarUrl: String): Observable<ResponseBody> {
        return githubApi.downloadAvatar(avatarUrl)
    }
}