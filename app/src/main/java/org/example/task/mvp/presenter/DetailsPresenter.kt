package org.example.task.mvp.presenter

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import android.content.Context
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.util.Log
import io.reactivex.schedulers.Schedulers
import org.example.task.Application
import org.example.task.R
import org.example.task.mvp.contract.DetailsContract
import org.example.task.mvp.model.network.NetworkApi
import org.example.task.mvp.presenter.base.MvpPresenterBase
import javax.inject.Inject

class DetailsPresenter @Inject constructor() : MvpPresenterBase<DetailsContract.View>(), DetailsContract.Presenter {
    companion object {
        private val TAG: String = DetailsPresenter::class.java.simpleName
    }

    /** Section: Injects. */

    @Inject
    lateinit var context: Context

    @Inject
    lateinit var networkApi: NetworkApi

    /** Section: Subjects. */

    private val detailsAvatar = MutableLiveData<Drawable>()

    /** Section: Constructor. */

    init {
        Application.component.inject(this)
    }

    /** Section: Overridden DetailsContract.Presenter methods. */

    override fun attachView(view: DetailsContract.View) {
        super.attachView(view)

        val lifecycleOwner = view.getLifecycleOwner()
        detailsAvatar.observe(lifecycleOwner, Observer {
            view.hideProgressBar()

            if (it != null) {
                view.showAvatar(it)
            } else {
                view.showErrorMessage(R.string.details_activity_error_avatar_message)
            }
        })
    }

    override fun detachView() {
        val view = getView()
        if (view != null) {
            val lifecycleOwner = view.getLifecycleOwner()
            detailsAvatar.removeObservers(lifecycleOwner)
        }

        super.detachView()
    }

    override fun readyView() {
        val view = getView() ?: return
        view.initialize()
    }

    override fun initialize(name: String, avatarUrl: String) {
        val view = getView() ?: return

        view.showName(name)
        view.showProgressBar()

        val observable = networkApi.asyncDownloadAvatar(avatarUrl)
        observable.subscribeOn(Schedulers.newThread())
                .observeOn(Schedulers.newThread())
                .subscribe({
                    val avatar = BitmapFactory.decodeStream(it.byteStream())
                    if (avatar == null) {
                        Log.w(TAG, "[asyncDownloadAvatar] Error while decoding stream.")
                        detailsAvatar.postValue(null)
                        return@subscribe
                    }

                    detailsAvatar.postValue(BitmapDrawable(context.resources, avatar))
                }, {
                    Log.w(TAG, "[asyncDownloadAvatar] Failure while downloading avatar. Throws: '" + it?.message + "'")
                    detailsAvatar.postValue(null)
                })
    }
}