package org.example.task.mvp.presenter

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import android.util.Log
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.example.task.Application
import org.example.task.R
import org.example.task.common.config.ApiConfig
import org.example.task.mvp.contract.SearchContract
import org.example.task.mvp.model.data.SearchItem
import org.example.task.mvp.model.data.SearchOwner
import org.example.task.mvp.model.network.NetworkApi
import org.example.task.mvp.presenter.base.MvpPresenterBase
import javax.inject.Inject

class SearchPresenter @Inject constructor() : MvpPresenterBase<SearchContract.View>(), SearchContract.Presenter {
    companion object {
        private val TAG: String = DetailsPresenter::class.java.simpleName
    }

    /** Section: Injects. */

    @Inject
    lateinit var networkApi: NetworkApi

    /** Section: Subjects. */

    private val searchResult = MutableLiveData<Pair<String, List<SearchItem>>>()
    private val searchPages = MutableLiveData<Pair<String, List<Int>>>()

    /** Section: Constructor. */

    init {
        Application.component.inject(this)
    }

    /** Section: Overridden SearchContract.Presenter methods. */

    override fun attachView(view: SearchContract.View) {
        super.attachView(view)

        val lifecycleOwner = view.getLifecycleOwner()
        searchResult.observe(lifecycleOwner, Observer {
            (getView() ?: return@Observer).searchResult(it?.first!!, it.second)
        })

        searchPages.observe(lifecycleOwner, Observer {
            (getView() ?: return@Observer).searchPages(it?.first!!, it.second)
        })
    }

    override fun detachView() {
        val view = getView()
        if (view != null) {
            val lifecycleOwner = view.getLifecycleOwner()
            searchResult.removeObservers(lifecycleOwner)
            searchPages.removeObservers(lifecycleOwner)
        }

        super.detachView()
    }

    override fun readyView() {
        val view = getView() ?: return
        view.initialize()
    }

    override fun search(query: String) {
        search(query, 1)
    }

    override fun search(query: String, page: Int) {
        val view = getView() ?: return
        if (query.isEmpty()) {
            view.showErrorMessage(R.string.search_activity_error_empty_field)
            return
        }

        view.showProgressBar()

        val observable = networkApi.asyncSearchRepository(query, page)
        observable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.hideProgressBar()
                    if (it.total == null || it.items == null) {
                        Log.w(TAG, "[asyncSearchRepository] Error while searching repositories: objects are null.")

                        view.showErrorMessage(R.string.search_activity_error_parse_error)
                        return@subscribe
                    }

                    view.clearPages()
                    view.clearResult()

                    if (it.items.isEmpty()) {
                        view.showErrorMessage(R.string.search_activity_error_empty_field)
                        return@subscribe
                    }

                    searchResult.value = Pair(query, it.items)

                    val totalPages = Math.min(it.total.toInt(), ApiConfig.GITHUB_ITEMS_PER_MAX_ITEMS) / ApiConfig.GITHUB_ITEMS_PER_PAGE + 1
                    searchPages.value = Pair(query, (1..totalPages).toList())
                }, {
                    Log.w(TAG, "[asyncSearchRepository] Failure while searching repositories. Throws: '" + it?.message + "'")

                    view.hideProgressBar()
                    view.showErrorMessage(R.string.search_activity_error_network_error)
                })
    }

    override fun details(owner: SearchOwner) {
        val view = getView() ?: return
        if (owner.name == null && owner.avatarUrl == null) {
            return
        }

        view.details(owner.name!!, owner.avatarUrl!!)
    }
}