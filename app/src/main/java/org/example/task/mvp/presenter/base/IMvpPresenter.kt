package org.example.task.mvp.presenter.base

import org.example.task.mvp.view.base.IMvpView

interface IMvpPresenter<V : IMvpView> {
    fun attachView(view: V)
    fun detachView()
    fun readyView()
    fun getView(): V?
}