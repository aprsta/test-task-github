package org.example.task.mvp.presenter.base

import android.support.annotation.CallSuper
import org.example.task.mvp.view.base.IMvpView
import java.lang.ref.WeakReference

abstract class MvpPresenterBase<V : IMvpView> : IMvpPresenter<V> {
    private var view: WeakReference<V>? = null
    private var ready: Boolean = false

    @CallSuper
    override fun attachView(view: V) {
        this.view = WeakReference(view)
    }

    @CallSuper
    override fun detachView() {
        this.view = null
    }

    override fun getView(): V? {
        return view?.get()
    }
}