package org.example.task.mvp.view

import android.content.res.Configuration
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.support.annotation.StringRes
import android.support.annotation.UiThread
import android.support.design.widget.Snackbar
import android.support.v4.widget.ContentLoadingProgressBar
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import org.example.task.Application
import org.example.task.R
import org.example.task.common.config.TransmittableKeys
import org.example.task.common.di.component.DaggerActivityComponent
import org.example.task.mvp.contract.DetailsContract
import org.example.task.mvp.view.base.MvpViewBase

class DetailsActivity : MvpViewBase<DetailsContract.View, DetailsContract.Presenter>(), DetailsContract.View {
    /** Section: Bind views. */

    @BindView(R.id.detailsParentView)
    lateinit var searchParentView: FrameLayout

    @BindView(R.id.detailsAvatar)
    lateinit var detailsAvatar: ImageView

    @BindView(R.id.detailsName)
    lateinit var detailsName: TextView

    @BindView(R.id.detailsProgressBarContainer)
    lateinit var detailsProgressBarContainer: FrameLayout

    @BindView(R.id.detailsProgressBar)
    lateinit var detailsProgressBar: ContentLoadingProgressBar

    /** Section: Lifecycle methods. */

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        if (intent == null || !intent.hasExtra(TransmittableKeys.GITHUB_NAME)
                || !intent.hasExtra(TransmittableKeys.GITHUB_AVATAR_URL)) {
            // GITHUB_NAME and GITHUB_AVATAR_URL required.
            assert(false)
        }

        ButterKnife.bind(this)

        presenter.readyView()

        val name = intent.getStringExtra(TransmittableKeys.GITHUB_NAME)
        val avatarUrl = intent.getStringExtra(TransmittableKeys.GITHUB_AVATAR_URL)
        presenter.initialize(name, avatarUrl)
    }

    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)

        // TODO: Handle configuration changed if needed.
    }

    /** Section: Overridden MvpViewBase methods. */

    override fun inject() {
        val component = DaggerActivityComponent.builder()
                .singletonComponent(Application.component)
                .build()

        component.inject(this)
    }

    /** Section: Overridden DetailsContract.View methods. */

    @UiThread
    override fun initialize() {
        hideProgressBar()
    }

    @UiThread
    override fun showProgressBar() {
        detailsProgressBarContainer.visibility = View.VISIBLE
        detailsProgressBar.show()
    }

    @UiThread
    override fun hideProgressBar() {
        detailsProgressBarContainer.visibility = View.GONE
        detailsProgressBar.hide()
    }

    @UiThread
    override fun showAvatar(drawable: Drawable) {
        detailsAvatar.setImageDrawable(drawable)
    }

    @UiThread
    override fun showName(name: String) {
        detailsName.text = name
    }

    @UiThread
    override fun showErrorMessage(@StringRes messageId: Int) {
        Snackbar.make(searchParentView, messageId, Snackbar.LENGTH_LONG).show()
    }
}
