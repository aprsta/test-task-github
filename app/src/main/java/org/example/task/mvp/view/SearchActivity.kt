package org.example.task.mvp.view

import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.support.annotation.StringRes
import android.support.annotation.UiThread
import android.support.design.widget.Snackbar
import android.support.v4.widget.ContentLoadingProgressBar
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.view.View
import android.widget.FrameLayout
import butterknife.BindView
import butterknife.ButterKnife
import org.example.task.Application
import org.example.task.R
import org.example.task.common.config.TransmittableKeys
import org.example.task.common.di.component.DaggerActivityComponent
import org.example.task.mvp.contract.SearchContract
import org.example.task.mvp.model.data.SearchItem
import org.example.task.mvp.view.adapter.SearchPagesAdapter
import org.example.task.mvp.view.adapter.SearchResultAdapter
import org.example.task.mvp.view.base.MvpViewBase

class SearchActivity : MvpViewBase<SearchContract.View, SearchContract.Presenter>(), SearchContract.View {
    /** Section: Bind views. */

    @BindView(R.id.searchParentView)
    lateinit var searchParentView: FrameLayout

    @BindView(R.id.searchView)
    lateinit var searchView: SearchView

    @BindView(R.id.searchResultRecyclerView)
    lateinit var searchResultRecyclerView: RecyclerView

    @BindView(R.id.searchPagesRecyclerView)
    lateinit var searchPagesRecyclerView: RecyclerView

    @BindView(R.id.searchProgressBarContainer)
    lateinit var searchProgressBarContainer: FrameLayout

    @BindView(R.id.searchProgressBar)
    lateinit var searchProgressBar: ContentLoadingProgressBar

    /** Section: Private fields. */

    private lateinit var searchResultAdapter: SearchResultAdapter

    private lateinit var searchPagesAdapter: SearchPagesAdapter

    /** Section: Lifecycle methods. */

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)

        ButterKnife.bind(this)

        searchPagesAdapter = SearchPagesAdapter(presenter)
        searchPagesRecyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        searchPagesRecyclerView.itemAnimator = DefaultItemAnimator()
        searchPagesRecyclerView.adapter = searchPagesAdapter

        searchResultAdapter = SearchResultAdapter(presenter)
        searchResultRecyclerView.layoutManager = LinearLayoutManager(this)
        searchResultRecyclerView.itemAnimator = DefaultItemAnimator()
        searchResultRecyclerView.adapter = searchResultAdapter

        presenter.readyView()
    }

    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)

        // TODO: Handle configuration changed if needed.
    }

    /** Section: Overridden MvpViewBase methods. */

    override fun inject() {
        val component = DaggerActivityComponent.builder()
                .singletonComponent(Application.component)
                .build()

        component.inject(this)
    }

    /** Section: Overridden SearchContract.View methods. */

    @UiThread
    override fun initialize() {
        hideProgressBar()
        hidePages()

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                presenter.search(query)
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean = false
        })
    }

    @UiThread
    override fun showProgressBar() {
        searchProgressBarContainer.visibility = View.VISIBLE
        searchProgressBar.show()
        searchView.isEnabled = false
        searchPagesRecyclerView.isEnabled = false
        searchResultRecyclerView.isEnabled = false
    }

    @UiThread
    override fun hideProgressBar() {
        searchProgressBarContainer.visibility = View.GONE
        searchProgressBar.hide()
        searchView.isEnabled = true
        searchPagesRecyclerView.isEnabled = true
        searchResultRecyclerView.isEnabled = true
    }

    @UiThread
    override fun showPages() {
        searchPagesRecyclerView.visibility = View.VISIBLE
    }

    @UiThread
    override fun hidePages() {
        searchPagesRecyclerView.visibility = View.GONE
    }

    @UiThread
    override fun clearResult() {
        searchResultAdapter.clear()
    }

    @UiThread
    override fun clearPages() {
        searchPagesAdapter.clear()
        hidePages()
    }

    @UiThread
    override fun searchResult(query: String, items: List<SearchItem>) {
        if (items.isEmpty()) {
            searchResultAdapter.clear()
        } else {
            searchResultAdapter.update(items)
        }
    }

    @UiThread
    override fun searchPages(query: String, items: List<Int>) {
        if (items.isEmpty()) {
            searchPagesAdapter.clear()
            hidePages()
        } else {
            searchPagesAdapter.update(query, items)
            showPages()
        }
    }

    @UiThread
    override fun details(name: String, avatarUrl: String) {
        val intent = Intent(this, DetailsActivity::class.java)
        intent.putExtra(TransmittableKeys.GITHUB_NAME, name)
        intent.putExtra(TransmittableKeys.GITHUB_AVATAR_URL, avatarUrl)
        startActivity(intent)
    }

    @UiThread
    override fun showErrorMessage(@StringRes messageId: Int) {
        Snackbar.make(searchParentView, messageId, Snackbar.LENGTH_LONG).show()
    }
}
