package org.example.task.mvp.view.adapter

import android.support.design.button.MaterialButton
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import org.example.task.R
import org.example.task.mvp.contract.SearchContract

class SearchPagesAdapter(private val presenter: SearchContract.Presenter) : RecyclerView.Adapter<SearchPagesAdapter.ViewHolder>() {
    private val items: MutableList<Int> = ArrayList()

    private var query = ""

    class ViewHolder(val button: MaterialButton) : RecyclerView.ViewHolder(button)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchPagesAdapter.ViewHolder {
        val viewGroup = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_search_pages, parent, false) as MaterialButton
        return ViewHolder(viewGroup)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.button.text = items[position].toString()
        holder.button.setOnClickListener {
            presenter.search(query, items[position])
        }
    }

    fun clear() {
        items.clear()
        notifyDataSetChanged()
    }

    fun update(query: String, data: List<Int>) {
        this.query = query

        items.clear()
        items.addAll(data)
        notifyDataSetChanged()
    }

    override fun getItemCount() = items.size
}