package org.example.task.mvp.view.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import org.example.task.R
import org.example.task.mvp.contract.SearchContract
import org.example.task.mvp.model.data.SearchItem

class SearchResultAdapter(private val presenter: SearchContract.Presenter) : RecyclerView.Adapter<SearchResultAdapter.ViewHolder>() {
    private val items: MutableList<SearchItem> = ArrayList()

    class ViewHolder(context: ViewGroup) : RecyclerView.ViewHolder(context) {
        val root = context
        val repositoryName: TextView = context.findViewById(R.id.repositoryName)
        val repositoryLanguage: TextView = context.findViewById(R.id.repositoryLanguage)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchResultAdapter.ViewHolder {
        val viewGroup = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_search_result, parent, false) as ViewGroup
        return ViewHolder(viewGroup)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.repositoryName.text = items[position].name
        holder.repositoryLanguage.text = items[position].language
        holder.root.setOnClickListener {
            val owner = items[position].owner ?: return@setOnClickListener
            presenter.details(owner)
        }
    }

    fun clear() {
        items.clear()
        notifyDataSetChanged()
    }

    fun update(data: List<SearchItem>) {
        items.clear()
        items.addAll(data)
        notifyDataSetChanged()
    }

    override fun getItemCount() = items.size
}