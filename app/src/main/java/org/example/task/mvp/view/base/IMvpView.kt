package org.example.task.mvp.view.base

import android.arch.lifecycle.LifecycleOwner

interface IMvpView {
    fun getLifecycleOwner(): LifecycleOwner
}