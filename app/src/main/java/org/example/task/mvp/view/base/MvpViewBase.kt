package org.example.task.mvp.view.base

import android.arch.lifecycle.LifecycleOwner
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import org.example.task.mvp.presenter.base.IMvpPresenter
import javax.inject.Inject

abstract class MvpViewBase<V : IMvpView, P : IMvpPresenter<V>> : AppCompatActivity(), IMvpView {
    @Inject
    lateinit var presenter: P

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        inject()

        @Suppress("UNCHECKED_CAST")
        presenter.attachView(this as V)
    }

    override fun onDestroy() {
        presenter.detachView()
        super.onDestroy()
    }

    override fun getLifecycleOwner(): LifecycleOwner = this

    abstract fun inject()
}